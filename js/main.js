const bookList = document.querySelector('.js-book-list');

if (books === undefined || books.lenght == 0){
    bookList.innerHTML +=
    `<tbody>
        <tr>
            <td>
                <h2>The list is empty - no books to display!</h2>
            </td>
        </tr>
    </tbody>`;
} else {
    books.forEach(book =>
        bookList.innerHTML +=
            `<tbody>
                <tr>
                    <td>${book.isbn}</td>
                    <td>${book.title}</td>
                    <td>${book.author}</td>
                    <td>${book.year}</td>
                    <td>${book.genre}</td>
                </tr>
            </tbody>`);
}