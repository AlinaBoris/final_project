books =[
    {
        isbn: 001,
        title: "Anna Karenina",
        author: "Leo Tolstoy",
        year: 1877,
        genre: "Drama"
    },
    {
        isbn: 002,
        title: "To Kill a Mockingbird",
        author: "Harper Lee",
        year: 1960,
        genre: "Bildungsroman"
    },
    {
        isbn: 003,
        title: "The Great Gatsby",
        author: "F. Scott Fitzgerald",
        year: 1925,
        genre: "Tragedy"
    },
    {
        isbn: 004,
        title: "One Hundred Years of Solitude",
        author: "Gabriel García Márquez",
        year: 1967,
        genre: "Magical realism"
    },
    {
        isbn: 005,
        title: "A Passage to India",
        author: "E.M. Forster",
        year: 1924,
        genre: "Political fiction"
    },
    {
        isbn: 006,
        title: "Invisible Man",
        author: "Ralph Ellison",
        year: 1897,
        genre: "Horror"
    },
    {
        isbn: 007,
        title: "Don Quixote",
        author: "Miguel de Cervantes",
        year: 1615,
        genre: "Comedy"
    }
]